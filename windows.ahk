; Helpful for Windows 10 users who are used to MacOS, and where special keys are not placed conveniently.
#Requires AutoHotkey v2.0
A_MaxHotkeysPerInterval := 25
#SingleInstance Force
#Warn
SetControlDelay 0 ; default 20
; END OF CONFIGURATIONS

; Alt+L > Alt+d (e.g. Microsoft Edge put cursor in URL box)
!L::Send "!d"

; Ctrl+Alt+n > n tilde
^!n::Send "ñ"

; Ctrl+Alt+Shift+Down > Ctrl+Shift+PgDn
^!+Down::Send "{Ctrl Down}{Shift Down}{PgDn}{Ctrl Up}{Shift Up}"

; Ctrl+Alt+Shift+Up > Ctrl+Shift+PgUp
^!+Up::Send "{Ctrl Down}{Shift Down}{PgUp}{Ctrl Up}{Shift Up}"

; Ctrl+Alt+Down > PageDown
^!Down:: Send "{PgDn}"

; Ctrl+Alt+Up > PageUp
^!Up:: Send "{PgUp}"

; Ctrl+Shift+Windows+Down > Ctrl+Shift+End
^+#Down:: Send "{Ctrl Down}{Shift Down}{End Down}{Ctrl Up}{Shift Up}{End Up}"

; Ctrl+Shift+Windows+Up > Ctrl+Shift+Home
^+#Up:: Send "{Ctrl Down}{Shift Down}{Home Down}{Ctrl Up}{Shift Up}{Home Up}"

; Ctrl+Shift+Windows+Right > Shift+End
^+#Right:: Send "{Shift Down}{End Down}{Shift Up}{End Up}"

; Ctrl+Shift+Windows+Left > Shift+Home
^+#Left:: Send "{Shift Down}{Home Down}{Shift Up}{Home Up}"

; Ctrl+Windows+Down > Ctrl+End
^#Down:: Send "{Ctrl Down}{End Down}{Ctrl Up}{End Up}"

; Ctrl+Windows+Up > Ctrl+Home
^#Up:: Send "{Ctrl Down}{Home Down}{Ctrl Up}{Home Up}"

; Ctrl+Backspace > MacOS Delete
^Backspace::Send "{Delete}"

; Ctrl+Windows+Right > End
^#Right:: Send "{End}"

; Ctrl+Windows+Left > Home
^#Left:: Send "{Home}"

; Windows+a > Ctrl+a MacOS Command+a Select All
#a:: Send "{Ctrl Down}a{Ctrl Up}"

; Windows+Backspace > Delete MacOS Delete
#Backspace:: Send "{Delete}"

; Windows+c > Ctrl+c MacOS Command+c Copy
#c:: Send "{Ctrl Down}c{Ctrl Up}"

; Windows+f > Ctrl+f MacOS Find
#f:: Send "{Ctrl Down}f{Ctrl Up}"

; Windows+q > Alt+F4 MacOS Command+q Quit
#q:: Send "{Alt Down}{F4}{Alt Up}"

; Windows+t > Ctrl+t MacOS Command+t New Tab
#t:: Send "{Ctrl Down}t{Ctrl Up}"

; Windows+v > Ctrl+v MacOS Command+v Paste
#v:: Send "{Ctrl Down}v{Ctrl Up}"

; Windows+w > Ctrl+w MacOS Command+w Close Tab
#w:: Send "{Ctrl Down}w{Ctrl Up}"

; Windows+y > Ctrl+y MacOS Command+z Redo
#y:: Send "{Ctrl Down}y{Ctrl Up}"

; Windows+z > Ctrl+z MacOS Command+z Undo
#z:: Send "{Ctrl Down}z{Ctrl Up}"

;*********CONFLICTS****************

; Ctrl+Down > PageDown
; Interferes with Excel default shortcuts
; ^Down:: Send "{PgDn}"

; Ctrl+Up > PageUp
; Interferes with Excel default shortcuts
; ^Up:: Send "{PgUp}"

; Windows+x > Ctrl+x MacOS Command+x Cut
; Conflicts with built-in Windows+x shortcut
; #x:: Send "{Ctrl Down}x{Ctrl Up}"
